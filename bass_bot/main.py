#!/usr/bin/env python3
import logging

from pythonjsonlogger import jsonlogger

from config import SERVER_FLAG
from server import serv_start
from app import bot

log = logging.getLogger()

class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        if log_record.get('level'):
            log_record['level'] = log_record['level'].upper()
        else:
            log_record['level'] = record.levelname


logHandler = logging.StreamHandler()
formatter = CustomJsonFormatter('%(level)s %(name)s %(message)s')
logHandler.setFormatter(formatter)
log.addHandler(logHandler)
log.setLevel(logging.INFO)


if __name__ == "__main__":
    if SERVER_FLAG:
        serv_start(bot)
        log.info('Running in server mode')
    else:
        bot.remove_webhook()
        log.info('Running in poll mode')
        bot.polling(none_stop=True)

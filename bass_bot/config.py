from os import getenv
from pathlib import Path

TOKEN = getenv('TOKEN')
assert TOKEN

DB_HOST = getenv('DB_HOST', 'localhost')
DB_PORT = getenv('DB_PORT', '5432')
DB_USER = getenv('DB_USER', 'postgres')
DB_PASSWORD = getenv("DB_PASSWORD", 'password')
DB_NAME = getenv('DB', 'postgres')

YT_LOGIN = getenv('YT_LOGIN')
YT_PASSWORD = getenv('YT_PASSWORD')

LOG_LEVEL = getenv('LOG_LEVEL', 'INFO')
LOG_SQL = bool(getenv('LOG_SQL', False))

SERVER_FLAG = getenv('SERVER_FLAG', False)

DOWNLOAD_PATH = Path(getenv('DOWNLOAD_PATH', '/opt/bass/download/'))
BASS_PATH = Path(getenv('BASS_PATH', "/opt/bass/boosted/"))
LOG_PATH = Path(getenv('LOG_FOLDER', '/var/log/bass_bot/'))

WH_HOST = getenv('WH_HOST', '')
WH_PORT = int(getenv("WH_PORT", "8443"))
WH_LISTEN = getenv('WH_LISTEN', '0.0.0.0')

WH_SSL_CERT = getenv("SSL_CERT", 'WH_cert.pem')
WH_SSL_PRIV = getenv('SSL_PRIV', 'WH_pkey.pem')  # TODO REMOVE?
WH_URL_BASE = "https://%s:%s" % (WH_HOST, WH_PORT)
WH_URL_PATH = "/%s/" % TOKEN
